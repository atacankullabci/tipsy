//
//  TipCalculator.swift
//  Tipsy
//
//  Created by Atacan Kullabcı on 12.05.2020.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation

struct BillCalculator {
    
    var payment: Payment?
    
    mutating func calculatePaymentPerPerson(totalPay: Float, split: String, tipPercent: Int) {
        let bill = Bill(totalPay: totalPay, split: split, tipPercent: tipPercent)
    
        payment = Payment(paymentPerPerson: calculatePaymentPerPerson(bill), description: createPaymentDescription(bill))
    }
    
    func calculatePaymentPerPerson(_ bill: Bill) -> Float {
        let tipPay = bill.totalPay * (Float(bill.tipPercent) / 100)
        
        return (bill.totalPay + tipPay) / Float(Int(bill.split) ?? -1)
    }
    
    func createPaymentDescription(_ bill: Bill) -> String {
        return "Split between \(bill.split) people, with \(bill.tipPercent)% tip."
    }
    
    func getSplitFormatted(splitAmount: Double) -> String{
        return String(Int(splitAmount))
    }
    
    func getTipPercentFormatted(_ selectedTip: String) -> Int {
        return Int(selectedTip.split(separator: "%")[0])!
    }
}
