//
//  Bill.swift
//  Tipsy
//
//  Created by Atacan Kullabcı on 12.05.2020.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation

struct Bill {
    var totalPay : Float
    var split : String
    var tipPercent: Int
}
