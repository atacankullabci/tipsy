//
//  Payment.swift
//  Tipsy
//
//  Created by Atacan Kullabcı on 17.05.2020.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation

struct Payment {
    var paymentPerPerson: Float
    var description: String
    
    init(paymentPerPerson: Float, description: String) {
        self.paymentPerPerson = paymentPerPerson
        self.description = description
    }
}
