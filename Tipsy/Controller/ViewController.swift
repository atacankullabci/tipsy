//
//  ViewController.swift
//  Tipsy
//
//  Created by Angela Yu on 09/09/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit

class BillInfoViewController: UIViewController {
    
    @IBOutlet weak var totalBill: UITextField!
    
    @IBOutlet weak var zeroPercentButton: UIButton!
    @IBOutlet weak var tenPercentButton: UIButton!
    @IBOutlet weak var twentyPercentButton: UIButton!
    
    @IBOutlet weak var splitCount: UILabel!
    
    var billCalculator : BillCalculator?
    
    var percentButtonsViews: UIView = UIView()
    
    var selectedTipPercent : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func tipSelected(_ sender: UIButton) {
        let selectedTip = getButtonTitle(button: sender)
        
        self.percentButtonsViews = sender.superview ?? UIView()
        
        if selectedTip == getButtonTitle(button: zeroPercentButton) {
            deselectOtherPercentButtons(otherThanThis: sender)
        } else if selectedTip == getButtonTitle(button: tenPercentButton) {
            deselectOtherPercentButtons(otherThanThis: sender)
        } else {
            deselectOtherPercentButtons(otherThanThis: sender)
        }
    }
    
    @IBAction func splitStepperPressed(_ sender: UIStepper) {
        self.splitCount.text = String(format: "%.0f", sender.value)
    }
    
    func getButtonTitle(button: UIButton) -> String {
        if let title = button.titleLabel?.text {
            return title
        } else {
            return ""
        }
    }
    
    func deselectOtherPercentButtons(otherThanThis: UIButton) {
        otherThanThis.isSelected = true
                
        for case let button as UIButton in self.percentButtonsViews.subviews {
            if getButtonTitle(button: otherThanThis) != getButtonTitle(button: button) {
                button.isSelected = false
            }
        }
    }
    
    @IBAction func calculateButtonPressed(_ sender: UIButton) {
    }
}

