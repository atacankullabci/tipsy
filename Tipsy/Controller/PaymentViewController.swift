//
//  PaymentViewController.swift
//  Tipsy
//
//  Created by Atacan Kullabcı on 13.05.2020.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import UIKit

class PaymentViewController: UIViewController {
    
    @IBOutlet weak var totalPaymentPerPerson: UILabel!
    @IBOutlet weak var paymentDesciption: UILabel!
    
    var payment = Payment(paymentPerPerson: 0.0, description: "")
	
    override func viewDidLoad() {
        super.viewDidLoad()

        totalPaymentPerPerson.text = String(payment.paymentPerPerson)
        paymentDesciption.text = payment.description
    }
    
    @IBAction func recalculatePayment(_ sender: UIButton) {
        // Return back to the bill view controller
        self.dismiss(animated: true, completion: nil)
    }
}
