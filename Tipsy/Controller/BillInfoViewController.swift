//
//  ViewController.swift
//  Tipsy
//
//  Created by Angela Yu on 09/09/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit

class BillInfoViewController: UIViewController {
    
    @IBOutlet weak var totalBill: UITextField!
    @IBOutlet weak var zeroPercentButton: UIButton!
    @IBOutlet weak var tenPercentButton: UIButton!
    @IBOutlet weak var twentyPercentButton: UIButton!
    @IBOutlet weak var splitCount: UILabel!
    
    var billCalculator = BillCalculator()
    
    var percentButtonsViews: UIView = UIView()
    
    var selectedTipPercent : Bool = false
    
    var totalPay : Float = 0.0
    var split : String = "2"
    var tipPercent: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func tipSelected(_ sender: UIButton) {
        let selectedTip = getButtonTitle(button: sender)
        tipPercent = billCalculator.getTipPercentFormatted(selectedTip)
        
        self.percentButtonsViews = sender.superview ?? UIView()
        
        if selectedTip == getButtonTitle(button: zeroPercentButton) {
            deselectOtherPercentButtons(otherThanThis: sender)
        } else if selectedTip == getButtonTitle(button: tenPercentButton) {
            deselectOtherPercentButtons(otherThanThis: sender)
        } else {
            deselectOtherPercentButtons(otherThanThis: sender)
        }
        
        totalBill.resignFirstResponder()
    }
    
    @IBAction func splitStepperPressed(_ sender: UIStepper) {
        split = billCalculator.getSplitFormatted(splitAmount: sender.value)
        self.splitCount.text = split
        
        totalBill.resignFirstResponder()
    }
    
    @IBAction func calculateButtonPressed(_ sender: UIButton) {
        totalPay = (totalBill.text! as NSString).floatValue
        
        billCalculator.calculatePaymentPerPerson(totalPay: totalPay, split: split, tipPercent: tipPercent)
        
        self.performSegue(withIdentifier: "goToPayment", sender: self)
    }
    
    // Page Transitioning
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToPayment" {
            let paymentViewController = segue.destination as! PaymentViewController
            
            paymentViewController.payment = billCalculator.payment ?? Payment(paymentPerPerson: 0.0, description: "")
            
        }
    }
    
    // Helper Functions
    func getButtonTitle(button: UIButton) -> String {
        if let title = button.titleLabel?.text {
            return title
        } else {
            return ""
        }
    }
    
    // Deselect the other buttons by looping through subviews
    func deselectOtherPercentButtons(otherThanThis: UIButton) {
        otherThanThis.isSelected = true
        
        for case let button as UIButton in self.percentButtonsViews.subviews {
            if getButtonTitle(button: otherThanThis) != getButtonTitle(button: button) {
                button.isSelected = false
            }
        }
    }
}

